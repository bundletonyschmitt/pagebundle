<?php

namespace TonySchmitt\PageBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use TonySchmitt\PageBundle\Entity\SimpleText;

class SimpleTextController extends Controller
{

  public function simpleTextAction($id, $args=array())
  {

    $em = $this->getDoctrine();

    $repository = $em->getRepository(SimpleText::class);

    $text = $repository->findOneBy(array("id" => $id))->getSimpleText();

    return $this->render('@TonySchmittPage/simpleText/text.html.twig',array('text' => $text));
  }
}
