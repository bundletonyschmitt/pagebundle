<?php

namespace TonySchmitt\PageBundle\Controller;

use EasyCorp\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;
use TonySchmitt\PageBundle\Entity\Module;
use TonySchmitt\PageBundle\Form\PageType;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;

class PageAdminController extends BaseAdminController
{

  protected function createEditForm($entity, array $entityProperties) {
    if(isset($_GET['provider']) && ($_GET['provider'] == 'blocks') ) {
      $repository = $this->getDoctrine()->getRepository(Module::class);
      $module = $repository->findAll();
      $listmodule = array();
      foreach ($module as $value) {
        $listmodule[] = array('id' => $value->getId(), 'name' => $value->getName(), 'options' => $value->getOptions());
      }

      return $this->get('form.factory')->create(PageType::class, $entity, array('modules' => $listmodule));
    } else {
      return parent::createEditForm($entity, $entityProperties);
    }
  }

  protected function updateEntity($entity) {
    if(isset($_GET["provider"]) && $_GET["provider"] == "blocks") {
      $blocks = $entity->getBlocks();
      $em = $this->getDoctrine()->getManager();

      foreach ($blocks as $value) {
        $controller_name = $value->getModule()->getOptions()[1];
        if($value->getOptions()->getId()) {
          $entity_blocks_options = $em->getRepository($controller_name)->find($value->getOptions()->getId());
        } else {
          $entity_blocks_options = new $controller_name();
        }

        foreach ((array) $value->getOptions() as $key_options => $val_options) {
          if($val_options || ltrim(str_replace($controller_name, "", $key_options)) != 'id') {
            $function = 'set'.ucfirst(ltrim(str_replace($controller_name, "", $key_options)));
            $entity_blocks_options->$function($val_options);
          }
        }

        $em->persist($entity_blocks_options);
        $em->flush();

        $value->setOptions($controller_name.','.$entity_blocks_options->getId());
        $value->setPage($entity);
      }
      $em->flush();
    }

    return parent::updateEntity($entity);
  }

  /**
   * The method that is executed when the user performs a 'edit' action on an entity.
   *
   * @return Response|RedirectResponse
   */
  protected function editAction()
  {
      $this->dispatch(EasyAdminEvents::PRE_EDIT);

      $id = $this->request->query->get('id');
      $easyadmin = $this->request->attributes->get('easyadmin');
      $entity = $easyadmin['item'];

      if ($this->request->isXmlHttpRequest() && $property = $this->request->query->get('property')) {
          $newValue = 'true' === mb_strtolower($this->request->query->get('newValue'));
          $fieldsMetadata = $this->entity['list']['fields'];

          if (!isset($fieldsMetadata[$property]) || 'toggle' !== $fieldsMetadata[$property]['dataType']) {
              throw new \RuntimeException(sprintf('The type of the "%s" property is not "toggle".', $property));
          }

          $this->updateEntityProperty($entity, $property, $newValue);

          // cast to integer instead of string to avoid sending empty responses for 'false'
          return new Response((int) $newValue);
      }

      $fields = $this->entity['edit']['fields'];

      $editForm = $this->executeDynamicMethod('create<EntityName>EditForm', array($entity, $fields));
      $deleteForm = $this->createDeleteForm($this->entity['name'], $id);

      $editForm->handleRequest($this->request);
      if ($editForm->isSubmitted() && $editForm->isValid()) {
        if(isset($_GET["provider"]) && $_GET["provider"] == "blocks" ) {
          $valid = false;
          if( !($entity->getBlocks()[0] == null || $entity->getBlocks()[0]->getName() == '')) {
            $valid = true;
          }
        } else {
          $valid = true;
        }
        if ($valid) {
          $this->dispatch(EasyAdminEvents::PRE_UPDATE, array('entity' => $entity));

          $this->executeDynamicMethod('preUpdate<EntityName>Entity', array($entity));
          $this->executeDynamicMethod('update<EntityName>Entity', array($entity));

          $this->dispatch(EasyAdminEvents::POST_UPDATE, array('entity' => $entity));

          return $this->redirectToReferrer();
        }
      }

      $this->dispatch(EasyAdminEvents::POST_EDIT);

      return $this->render($this->entity['templates']['edit'], array(
          'form' => $editForm->createView(),
          'entity_fields' => $fields,
          'entity' => $entity,
          'delete_form' => $deleteForm->createView(),
          'options' => $editForm,
      ));
  }

}
