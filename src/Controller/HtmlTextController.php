<?php

namespace TonySchmitt\PageBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use TonySchmitt\PageBundle\Entity\HtmlText;

class HtmlTextController extends Controller
{

  public function HtmlTextAction($id, $args=array())
  {

    $em = $this->getDoctrine();

    $repository = $em->getRepository(htmlText::class);

    $text = $repository->findOneBy(array("id" => $id))->getHtmlText();

    return $this->render('@TonySchmittPage/htmlText/text.html.twig',array('text' => $text));
  }
}
