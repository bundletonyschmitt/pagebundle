<?php

namespace TonySchmitt\PageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use TonySchmitt\PageBundle\Entity\Page;

class DefaultController extends Controller
{

  const PAGE_NAME = '';

  public function indexAction($page)
  {
    $page = '/'.$page;

    if($page[strlen($page)-1] == '/' && strlen($page) > 1)
    {
      $page = substr($page, 0, -1);
    }

    $pageBDD = false;

    $i=0;
    $args = array();
    while (!$pageBDD) {
      if($i > 0) {
        $lastPos = strrpos($urlPage, '/');
        if($lastPos == 0) {
          break;
        }

        array_unshift($args, substr($urlPage, $lastPos + 1));
        $urlPage = substr($urlPage, 0, $lastPos);

        $url = $urlPage;

        for($j=1; $j <= $i; $j++) {
          $url .= '/{'.$j.'}';
        }

      } else {
        $urlPage = $page;
        $url = $urlPage;
      }

      $repositoryPage = $this->getDoctrine()->getRepository(Page::class);

      $pageBDD = $repositoryPage->findOneBy(array("url" => $url));

      if(!$pageBDD) {
        $query = $repositoryPage->createQueryBuilder('a')
                   ->where('a.url LIKE :url')
                   ->setParameter('url', '%'.$urlPage.'%')
                   ->getQuery();
        $result = $query->getResult();
        if(count($result) > 0)
          $pageBDD = $result[0];
      }

      $i++;

    }

    return $this->renderPage($pageBDD, $this, $args);

    die();
  }

  public function renderPage($pageBDD, $th, $args = array(), $variableRender = array()) {
    if($pageBDD && $pageBDD->getIsEnabled()) {
      define('PAGE_NAME', $pageBDD->getName());
      $templateRender = $pageBDD->getTemplate()->getTemplate()->getTemplate();
      $templateBlock = $pageBDD->getTemplate()->getTemplate()->getBlock();
      $variableRender['metaBaliseHead'] = $this->getMetaBaliseHead($pageBDD);
      foreach ($pageBDD->getBlocks() as $value) {
        $controller_name = $value->getModule()->getOptions()[2];
        $function = $value->getModule()->getOptions()[3];
        $block = $templateBlock[$value->getParentTemplate()];
        $id = explode(",", $value->getOptions())[1];

        $render = $th->forward($controller_name.'::'.$function, array('id' => $id, 'args' => $args));

        if(!isset($variableRender[$block])) {
          $variableRender[$block] = '';
        }
        $contentTemplate = $value->getContentTemplate();
        $variableRender[$block] .= $contentTemplate->getBeforeContent() . $render->getContent() . $contentTemplate->getAfterContent();
      }


      $templateBlockPage = $pageBDD->getTemplate()->getTemplate()->getBlockPage();
      foreach ($pageBDD->getTemplate()->getTemplateBlocks() as $value) {
        $controller_name = $value->getModule()->getOptions()[2];
        $function = $value->getModule()->getOptions()[3];
        $block = $templateBlockPage[$value->getParentTemplate()];
        $id = explode(",", $value->getOptions())[1];

        $render = $th->forward($controller_name.'::'.$function, array('id' => $id, 'args' => $args));

        if(!isset($variableRender[$block])) {
          $variableRender[$block] = '';
        }
        $variableRender[$block] .= $render->getContent();
      }
      
      return $th->render($templateRender, $variableRender);
    } else {
      echo 'Error 404';die();
    }
  }

  public function getMetaBaliseHead($page) {
    // title
    $balise = array();
    $title = $page->getSeoTitle();
    if($title != NULL && $title != '') {
      $balise['title'] = $title;
    } else {
      $balise['title'] = $page->getName();
    }

    $balise['meta'] = '';

    $balise['meta'] .= $this->transformToMetaName("description", $page->getSeoDescription())
    . $this->transformToMetaProperty("og:url", (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]")
    . $this->transformToMetaProperty("og:type", "website")
    . $this->transformToMetaProperty("og:title", $balise['title'])
    . $this->transformToMetaProperty("og:description", $page->getSeoDescription())
    . $this->transformToMetaProperty("og:image", (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]" . $page->getSeoMedia()->getDirectory() . $page->getSeoMedia()->getMedia())
    . $this->transformToMetaName("twitter:card", "summary")
    . $this->transformToMetaName("twitter:title", $balise['title'])
    . $this->transformToMetaName("twitter:description", $page->getSeoDescription())
    . $this->transformToMetaName("twitter:image", (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]" . $page->getSeoMedia()->getDirectory() . $page->getSeoMedia()->getMedia());

    return $balise;
  }

  public function transformToMetaName($name, $content) {
    return '<meta name="'.$name.'" content="'.htmlentities($content).'" />';
  }

  public function transformToMetaProperty($property, $content) {
    return '<meta property="'.$property.'" content="'.htmlentities($content).'" />';
  }
}
