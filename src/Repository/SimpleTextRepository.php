<?php

namespace TonySchmitt\PageBundle\Repository;

use TonySchmitt\PageBundle\Entity\SimpleText;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class SimpleTextRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SimpleText::class);
    }
}
