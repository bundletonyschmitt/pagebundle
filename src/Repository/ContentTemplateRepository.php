<?php

namespace TonySchmitt\PageBundle\Repository;

use TonySchmitt\PageBundle\Entity\ContentTemplate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ContentTemplateRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ContentTemplate::class);
    }
}
