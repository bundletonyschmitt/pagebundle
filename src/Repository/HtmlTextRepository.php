<?php

namespace TonySchmitt\PageBundle\Repository;

use TonySchmitt\PageBundle\Entity\HtmlText;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class HtmlTextRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, HtmlText::class);
    }
}
