<?php

namespace TonySchmitt\PageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="TonySchmitt\PageBundle\Repository\ContentTemplateRepository")
 */
class ContentTemplate
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="beforeContent", type="text", nullable=true)
     */
    private $beforeContent;

    /**
     * @var string
     *
     * @ORM\Column(name="afterContent", type="text", nullable=true)
     */
    private $afterContent;

    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param mixed id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of Name
     *
     * @param string name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function __toString()
    {
      return $this->name;
    }

    /**
     * Get the value of beforeContent
     *
     * @return  string
     */ 
    public function getBeforeContent()
    {
        return $this->beforeContent;
    }

    /**
     * Set the value of beforeContent
     *
     * @param  string  $beforeContent
     *
     * @return  self
     */ 
    public function setBeforeContent($beforeContent)
    {
        $this->beforeContent = $beforeContent;

        return $this;
    }

    /**
     * Get the value of afterContent
     *
     * @return  string
     */ 
    public function getAfterContent()
    {
        return $this->afterContent;
    }

    /**
     * Set the value of afterContent
     *
     * @param  string  $afterContent
     *
     * @return  self
     */ 
    public function setAfterContent($afterContent)
    {
        $this->afterContent = $afterContent;

        return $this;
    }
}
