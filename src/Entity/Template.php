<?php

namespace TonySchmitt\PageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="TonySchmitt\PageBundle\Repository\TemplateRepository")
 */
class Template
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="template", type="string", length=255)
     */
    private $template;

    /**
     * @var array
     *
     * @ORM\Column(name="block", type="array")
     */
    private $block;

    /**
     * @var array
     *
     * @ORM\Column(name="blockPage", type="array")
     */
    private $blockPage;

    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param mixed id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of Name
     *
     * @param string name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of Template
     *
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Set the value of Template
     *
     * @param string template
     *
     * @return self
     */
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Get the value of Block
     *
     * @return array
     */
    public function getBlock()
    {
        if(is_array($this->block)) {
          ksort($this->block);
        }

        return $this->block;
    }

    /**
     * Set the value of Block
     *
     * @param array block
     *
     * @return self
     */
    public function setBlock(array $block)
    {
        $this->block = $block;

        return $this;
    }

    public function __toString()
    {
      return $this->name;
    }


    /**
     * Get the value of blockPage
     *
     * @return  array
     */ 
    public function getBlockPage()
    {
        return $this->blockPage;
    }

    /**
     * Set the value of blockPage
     *
     * @param  array  $blockPage
     *
     * @return  self
     */ 
    public function setBlockPage(array $blockPage)
    {
        $this->blockPage = $blockPage;

        return $this;
    }
}
