<?php

namespace TonySchmitt\PageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="TonySchmitt\PageBundle\Repository\TemplateBlockRepository")
 */
class TemplateBlock
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="parentTemplate", type="string", length=255)
     */
    private $parentTemplate;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * @ORM\ManyToOne(targetEntity="TonySchmitt\PageBundle\Entity\TemplatePage", inversedBy="templateBlocks")
     */
    private $templatePage;

    /**
     * @ORM\ManyToOne(targetEntity="TonySchmitt\PageBundle\Entity\Module")
     * @ORM\JoinColumn(nullable=true)
     */
    private $module;

    /**
     * @var array
     *
     * @ORM\Column(name="options", type="array", nullable=true)
     */
    private $options;

    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param mixed id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of Name
     *
     * @param string name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of ParentTemplate
     *
     * @return string
     */
    public function getParentTemplate()
    {
        return $this->parentTemplate;
    }

    /**
     * Set the value of ParentTemplate
     *
     * @param string parentTemplate
     *
     * @return self
     */
    public function setParentTemplate($parentTemplate)
    {
        $this->parentTemplate = $parentTemplate;

        return $this;
    }

    /**
     * Get the value of Position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set the value of Position
     *
     * @param integer position
     *
     * @return self
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get the value of Module
     *
     * @return mixed
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Set the value of Module
     *
     * @param mixed module
     *
     * @return self
     */
    public function setModule($module)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get the value of Options
     *
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set the value of Options
     *
     * @param array options
     *
     * @return self
     */
    public function setOptions($options)
    {
        $this->options = $options;

        return $this;
    }


    /**
     * Get the value of templatePage
     */ 
    public function getTemplatePage()
    {
        return $this->templatePage;
    }

    /**
     * Set the value of templatePage
     *
     * @return  self
     */ 
    public function setTemplatePage($templatePage)
    {
        $this->templatePage = $templatePage;

        return $this;
    }
}
