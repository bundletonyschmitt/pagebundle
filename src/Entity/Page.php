<?php

namespace TonySchmitt\PageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="TonySchmitt\PageBundle\Repository\PageRepository")
 */
class Page
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isEnabled", type="boolean", nullable=true)
     */
    private $isEnabled;

    /**
     * @var integer
     *
     * @ORM\Column(name="weight", type="integer", nullable=true)
     */
    private $weight;

    /**
     * @var string
     *
     * @ORM\Column(name="seoTitle", type="string", length=255, nullable=true)
     */
    private $seoTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="seoDescription", type="string", length=511, nullable=true)
     */
    private $seoDescription;

    /**
     * @ORM\ManyToOne(targetEntity="TonySchmitt\MediaBundle\Entity\Media", cascade={"persist"})
     */
    private $seoMedia;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * @ORM\ManyToOne(targetEntity="TonySchmitt\PageBundle\Entity\TemplatePage", cascade={"persist"})
     */
    private $template;

    /**
     * @ORM\OneToMany(targetEntity="TonySchmitt\PageBundle\Entity\Block", mappedBy="page", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\JoinColumn(nullable=true)
     * @ORM\OrderBy({"position" = "asc"})
     */
    private $blocks;

    public function __construct()
    {
      $this->blocks = new ArrayCollection();
    }


    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param mixed id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of Name
     *
     * @param string name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of Url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set the value of Url
     *
     * @param string url
     *
     * @return self
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get the value of IsEnabled
     *
     * @return boolean
     */
    public function getIsEnabled()
    {
        return $this->isEnabled;
    }

    /**
     * Set the value of IsEnabled
     *
     * @param boolean isEnabled
     *
     * @return self
     */
    public function setIsEnabled($isEnabled)
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    /**
     * Get the value of Weight
     *
     * @return boolean
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set the value of Weight
     *
     * @param boolean weight
     *
     * @return self
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get the value of SeoTitle
     *
     * @return boolean
     */
    public function getSeoTitle()
    {
        return $this->seoTitle;
    }

    /**
     * Set the value of SeoTitle
     *
     * @param boolean seoTitle
     *
     * @return self
     */
    public function setSeoTitle($seoTitle)
    {
        $this->seoTitle = $seoTitle;

        return $this;
    }

    /**
     * Get the value of SeoDescription
     *
     * @return boolean
     */
    public function getSeoDescription()
    {
        return $this->seoDescription;
    }

    /**
     * Set the value of SeoDescription
     *
     * @param boolean seoDescription
     *
     * @return self
     */
    public function setSeoDescription($seoDescription)
    {
        $this->seoDescription = $seoDescription;

        return $this;
    }

    /**
     * Get the value of Content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set the value of Content
     *
     * @param string content
     *
     * @return self
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get the value of Template
     *
     * @return mixed
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Set the value of Template
     *
     * @param mixed template
     *
     * @return self
     */
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

  /**
   * Add block
   *
   * @param \TonySchmitt\PageBundle\Entity\Block $block
   *
   * @return Block
   */
  public function addBlock(Block $block)
  {

    $block->setPage($this);

    $this->blocks->add($block);

    return $this;
  }

  public function removeBlock(Block $block)
  {
    $this->blocks->removeElement($block);
  }

  /**
   * @return Collection|Block[]
   */
  public function getBlocks()
  {
    return $this->blocks;
  }
  
    /**
     * Get the value of seoMedia
     */ 
    public function getSeoMedia()
    {
        return $this->seoMedia;
    }

    /**
     * Set the value of seoMedia
     *
     * @return  self
     */ 
    public function setSeoMedia($seoMedia)
    {
        $this->seoMedia = $seoMedia;

        return $this;
    }

    public function __toString() {
        return $this->name .' ('. $this->url.')';
    }
}
