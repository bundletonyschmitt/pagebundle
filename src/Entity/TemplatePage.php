<?php

namespace TonySchmitt\PageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="TonySchmitt\PageBundle\Repository\TemplatePageRepository")
 */
class TemplatePage
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="TonySchmitt\PageBundle\Entity\Template", cascade={"persist"})
     */
    private $template;

    /**
     * @ORM\OneToMany(targetEntity="TonySchmitt\PageBundle\Entity\TemplateBlock", mappedBy="templatePage", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\JoinColumn(nullable=true)
     * @ORM\OrderBy({"position" = "asc"})
     */
    private $templateBlocks;

    public function __construct()
    {
      $this->templateBlocks = new ArrayCollection();
    }


    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of name
     *
     * @return  string
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @param  string  $name
     *
     * @return  self
     */ 
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of template
     */ 
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Set the value of template
     *
     * @return  self
     */ 
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Add templateBlock
     *
     * @param \TonySchmitt\PageBundle\Entity\TemplateBlock $templateBlock
     *
     * @return TemplateBlock
     */
    public function addTemplateBlock(TemplateBlock $templateBlock)
    {

        $templateBlock->setTemplatePage($this);

        $this->templateBlocks->add($templateBlock);

        return $this;
    }

    public function removeTemplateBlock(TemplateBlock $templateBlock)
    {
        $this->templateBlocks->removeElement($templateBlock);
    }

    /**
     * @return Collection|TemplateBlock[]
     */
    public function getTemplateBlocks()
    {
        return $this->templateBlocks;
    }

    public function __toString()
    {
      return $this->name;
    }
}