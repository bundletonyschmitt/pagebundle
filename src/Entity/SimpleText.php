<?php

namespace TonySchmitt\PageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="TonySchmitt\PageBundle\Repository\SimpleTextRepository")
 */
class SimpleText
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="simpleText", type="text")
     */
    private $simpleText;

    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param mixed id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Simple Text
     *
     * @return string
     */
    public function getSimpleText()
    {
        return $this->simpleText;
    }

    /**
     * Set the value of Simple Text
     *
     * @param string simpleText
     *
     * @return self
     */
    public function setSimpleText($simpleText)
    {
        $this->simpleText = $simpleText;

        return $this;
    }

}
