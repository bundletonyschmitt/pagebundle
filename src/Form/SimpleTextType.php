<?php

namespace TonySchmitt\PageBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use TonySchmitt\PageBundle\Entity\SimpleText;
// ... use FormType... (text, choice...)

class SimpleTextType extends AbstractType {

  public function buildForm(FormBuilderInterface $builder, array $options) {

    $builder
    ->add('id', HiddenType::class, array('required' => false))
    ->add('simpleText', TextareaType::class, array('label' => 'Nom :'))
    ;
  }

  /**
   * @param OptionsResolver $resolver
   */
  public function configureOptions(OptionsResolver $resolver)
  {
      $resolver->setDefaults(array(
          'data_class' => 'TonySchmitt\PageBundle\Entity\SimpleText'
      ));
  }
}
