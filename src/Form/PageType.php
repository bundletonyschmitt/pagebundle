<?php

namespace TonySchmitt\PageBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use TonySchmitt\PageBundle\Form\Type\BlockCollectionType;
use TonySchmitt\PageBundle\Form\BlockType;
use TonySchmitt\PageBundle\Entity\Module;
// ... use FormType... (text, choice...)

class PageType extends AbstractType {

  public function buildForm(FormBuilderInterface $builder, array $options) {
    $builder
    ->add('blocks', BlockCollectionType::class, array(
      'entry_type' => BlockType::class,
      'entry_options'  => array(
        'blocks' => array_flip($options["data"]->getTemplate()->getTemplate()->getBlock()),
        'modules' => $options["modules"],
        'required' => false
      ),
      'required' => false,
      'allow_delete' => true,
      'allow_add' => true,
      'label' => 'Block list :'
      )
    )
    ;
  }

  /**
   * @param OptionsResolver $resolver
   */
  public function configureOptions(OptionsResolver $resolver)
  {
      $resolver->setDefaults(array(
          'data_class' => 'TonySchmitt\PageBundle\Entity\Page',
          'modules' => array()
      ));
  }
}
