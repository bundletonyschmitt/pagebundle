<?php

namespace TonySchmitt\PageBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use TonySchmitt\PageBundle\Entity\HtmlText;
use TonySchmitt\PageBundle\Form\Type\TextareaTinyMCEType;
// ... use FormType... (text, choice...)

class HtmlTextType extends AbstractType {

  public function buildForm(FormBuilderInterface $builder, array $options) {

    $builder
    ->add('id', HiddenType::class, array('required' => false))
    ->add('htmlText', TextareaTinyMCEType::class, array('label' => 'Nom :', 'attr' => array('class' => 'textarea-tinymce')))
    ;
  }

  /**
   * @param OptionsResolver $resolver
   */
  public function configureOptions(OptionsResolver $resolver)
  {
      $resolver->setDefaults(array(
          'data_class' => 'TonySchmitt\PageBundle\Entity\HtmlText'
      ));
  }
}
