<?php

namespace TonySchmitt\PageBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class TextareaTinyMCEType extends AbstractType
{
    public function getParent()
    {
        return TextareaType::class;
    }

    public function getBlockPrefix()
    {
        return 'tonyschmitt_page_block_textarea_tinymce';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }
}
