<?php

namespace TonySchmitt\PageBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class BlockCollectionType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'multiple' => true,
            'data_class' => null,
            'allow_add' => true,
            'allow_remove' => true
        ));
    }

    public function getParent()
    {
        return CollectionType::class;
    }

    public function getBlockPrefix()
    {
        return 'tonyschmitt_page_block_collection';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }
}
