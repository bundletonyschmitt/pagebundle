<?php

namespace TonySchmitt\PageBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use TonySchmitt\PageBundle\Entity\Module;
use Doctrine\Common\Persistence\ManagerRegistry as Doctrine;
// ... use FormType... (text, choice...)

class BlockType extends AbstractType {

  /** @var \Doctrine\ORM\EntityManager */
  private $em;

  /**
   * Constructor
   *
   * @param Doctrine $doctrine
   */
  public function __construct(Doctrine $doctrine)
  {
      $this->em = $doctrine->getManager();
  }

  public function buildForm(FormBuilderInterface $builder, array $options) {

    $blocks = array();
    if(isset($options['blocks'])) {
      $blocks = $options['blocks'];
    }

    $builder
    ->add('name', TextType::class, array('label' => 'Nom :'))
    ->add('parentTemplate', ChoiceType::class, array(
      'choices' => $blocks,
      'label' => 'Parent :'
    ))
    ->add('contentTemplate')
    ->add('position', IntegerType::class, array('label' => 'Position :'))
    ->add('module', EntityType::class, array('class' => Module::class, 'label' => ' ', 'attr' => array('style' => 'display: none;')))
    ;


    $formModifier = function (FormInterface $form, Module $module = null, $data = null) {
        $options = null === $module ? array() : $module->getOptions();

        if($options == null || count($options) <= 0) {
          $options[0] = 'Symfony\Component\Form\Extension\Core\Type\HiddenType';
        }
        $label = array('Options');
        if($data) {
          $data = $this->em->getRepository($module->getOptions()[1])->find(explode(',',$data->getOptions())[1]);
        }
        $label = explode('\\',$options[0]);
          $form->add('options', $options[0], array(
            'label' => substr($label[count($label) - 1], 0, -4).' :', 'data' => $data,
            'attr' => array('style' => 'margin-left: 50px;'))
          );

    };

    $builder->addEventListener(
        FormEvents::PRE_SET_DATA,
        function (FormEvent $event) use ($formModifier) {
            // this would be your entity, i.e. SportMeetup
            $data = $event->getData();
            if($data) {
              $module = $data->getModule();
            } else {
              $module = new Module();
            }

            $formModifier($event->getForm(), $module, $data);
        }
    );

    $builder->get('module')->addEventListener(
        FormEvents::POST_SUBMIT,
        function (FormEvent $event) use ($formModifier) {
            // It's important here to fetch $event->getForm()->getData(), as
            // $event->getData() will get you the client data (that is, the ID)
            $module = $event->getForm()->getData();

            // since we've added the listener to the child, we'll have to pass on
            // the parent to the callback functions!
            $formModifier($event->getForm()->getParent(), $module);
        }
    );
  }

  /**
   * @param OptionsResolver $resolver
   */
  public function configureOptions(OptionsResolver $resolver)
  {
      $resolver->setDefaults(array(
          'data_class' => 'TonySchmitt\PageBundle\Entity\Block',
          'blocks' => array(),
          'modules' => array()
      ));
  }
}
