After installation add this lines in the end of routes.yaml :

"
tonyschmitt_pages:
    path:     /{page}
    defaults: { _controller: TonySchmitt\PageBundle\Controller\DefaultController::indexAction, page: '' }
    requirements:
        page: .+
"
